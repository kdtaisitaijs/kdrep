﻿namespace masterVersion
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.BTTN_OpenTestGen = new System.Windows.Forms.Button();
            this.BTTN_CloseMain = new System.Windows.Forms.Button();
            this.BTTN_OpenTaskView = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BTTN_ImportTetst = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BTTN_OpenTestGen
            // 
            this.BTTN_OpenTestGen.Location = new System.Drawing.Point(12, 11);
            this.BTTN_OpenTestGen.Name = "BTTN_OpenTestGen";
            this.BTTN_OpenTestGen.Size = new System.Drawing.Size(246, 73);
            this.BTTN_OpenTestGen.TabIndex = 0;
            this.BTTN_OpenTestGen.Text = "Veidot Kontroldarbu";
            this.BTTN_OpenTestGen.UseVisualStyleBackColor = true;
            this.BTTN_OpenTestGen.Click += new System.EventHandler(this.BTTN_OpenTestGen_Click);
            // 
            // BTTN_CloseMain
            // 
            this.BTTN_CloseMain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BTTN_CloseMain.Location = new System.Drawing.Point(742, 437);
            this.BTTN_CloseMain.Name = "BTTN_CloseMain";
            this.BTTN_CloseMain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BTTN_CloseMain.Size = new System.Drawing.Size(124, 56);
            this.BTTN_CloseMain.TabIndex = 1;
            this.BTTN_CloseMain.Text = "Iziet";
            this.BTTN_CloseMain.UseVisualStyleBackColor = true;
            this.BTTN_CloseMain.Click += new System.EventHandler(this.BTTN_CloseMain_Click);
            // 
            // BTTN_OpenTaskView
            // 
            this.BTTN_OpenTaskView.Location = new System.Drawing.Point(12, 109);
            this.BTTN_OpenTaskView.Name = "BTTN_OpenTaskView";
            this.BTTN_OpenTaskView.Size = new System.Drawing.Size(246, 73);
            this.BTTN_OpenTaskView.TabIndex = 2;
            this.BTTN_OpenTaskView.Text = "Pievienot Uzdevumu";
            this.BTTN_OpenTaskView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BTTN_OpenTaskView.UseVisualStyleBackColor = true;
            this.BTTN_OpenTaskView.Click += new System.EventHandler(this.BTTN_OpenTaskView_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(264, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(586, 409);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // BTTN_ImportTetst
            // 
            this.BTTN_ImportTetst.Location = new System.Drawing.Point(12, 210);
            this.BTTN_ImportTetst.Name = "BTTN_ImportTetst";
            this.BTTN_ImportTetst.Size = new System.Drawing.Size(246, 73);
            this.BTTN_ImportTetst.TabIndex = 4;
            this.BTTN_ImportTetst.Text = "Importēt Kontroldarbu";
            this.BTTN_ImportTetst.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BTTN_ImportTetst.UseVisualStyleBackColor = true;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 505);
            this.Controls.Add(this.BTTN_ImportTetst);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BTTN_OpenTaskView);
            this.Controls.Add(this.BTTN_CloseMain);
            this.Controls.Add(this.BTTN_OpenTestGen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "main";
            this.Tag = "main";
            this.Text = "Kontroldarbu veidotājs";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BTTN_OpenTestGen;
        private System.Windows.Forms.Button BTTN_CloseMain;
        private System.Windows.Forms.Button BTTN_OpenTaskView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BTTN_ImportTetst;
    }
}

