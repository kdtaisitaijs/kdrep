﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class TestGenMaster : Form
    {
        main mainForm;


        public TestGenMaster(main _main)
        {
            InitializeComponent();
            mainForm = _main;
            


        }

        private void TestGenMaster_Load(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void BTTN_ChseGrade10_Click(object sender, EventArgs e)
        {
            checkBox1.Hide();
            checkBox3.Show();
            checkBox4.Hide();
        }

        private void BTTN_backToMain_Click(object sender, EventArgs e)
        {
            mainForm.Show();
            this.Hide();

        }

        private void BTTN_ChseGrade8_Click(object sender, EventArgs e)
        {
            checkBox1.Show();
            checkBox3.Hide();
            checkBox4.Hide();
        }

        private void BTTN_ChseGrade9_Click(object sender, EventArgs e)
        {
            checkBox1.Hide();
            checkBox3.Hide();
            checkBox4.Show();
        }
    }
}
