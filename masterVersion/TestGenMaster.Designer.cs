﻿namespace masterVersion
{
    partial class TestGenMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.BTTN_ChseGrade8 = new System.Windows.Forms.Button();
            this.BTTN_ChseGrade9 = new System.Windows.Forms.Button();
            this.BTTN_ChseGrade10 = new System.Windows.Forms.Button();
            this.BTTN_ChseGrade11 = new System.Windows.Forms.Button();
            this.BTTN_ChseGrade12 = new System.Windows.Forms.Button();
            this.BTTN_backToMain = new System.Windows.Forms.Button();
            this.BTTN_viewDatabase = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.BTTN_GenSettings = new System.Windows.Forms.Button();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(661, 413);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 58);
            this.button1.TabIndex = 0;
            this.button1.Text = "Izveidot Kontroldarbu";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(488, 413);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(154, 58);
            this.button2.TabIndex = 1;
            this.button2.Text = "Aplūkot Kontroldarbu";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // BTTN_ChseGrade8
            // 
            this.BTTN_ChseGrade8.Location = new System.Drawing.Point(25, 26);
            this.BTTN_ChseGrade8.Name = "BTTN_ChseGrade8";
            this.BTTN_ChseGrade8.Size = new System.Drawing.Size(124, 47);
            this.BTTN_ChseGrade8.TabIndex = 2;
            this.BTTN_ChseGrade8.Text = "8. klase";
            this.BTTN_ChseGrade8.UseVisualStyleBackColor = true;
            this.BTTN_ChseGrade8.Click += new System.EventHandler(this.BTTN_ChseGrade8_Click);
            // 
            // BTTN_ChseGrade9
            // 
            this.BTTN_ChseGrade9.Location = new System.Drawing.Point(25, 101);
            this.BTTN_ChseGrade9.Name = "BTTN_ChseGrade9";
            this.BTTN_ChseGrade9.Size = new System.Drawing.Size(124, 47);
            this.BTTN_ChseGrade9.TabIndex = 3;
            this.BTTN_ChseGrade9.Text = "9, klase";
            this.BTTN_ChseGrade9.UseVisualStyleBackColor = true;
            this.BTTN_ChseGrade9.Click += new System.EventHandler(this.BTTN_ChseGrade9_Click);
            // 
            // BTTN_ChseGrade10
            // 
            this.BTTN_ChseGrade10.Location = new System.Drawing.Point(25, 180);
            this.BTTN_ChseGrade10.Name = "BTTN_ChseGrade10";
            this.BTTN_ChseGrade10.Size = new System.Drawing.Size(124, 47);
            this.BTTN_ChseGrade10.TabIndex = 4;
            this.BTTN_ChseGrade10.Text = "10. klase";
            this.BTTN_ChseGrade10.UseVisualStyleBackColor = true;
            this.BTTN_ChseGrade10.Click += new System.EventHandler(this.BTTN_ChseGrade10_Click);
            // 
            // BTTN_ChseGrade11
            // 
            this.BTTN_ChseGrade11.Location = new System.Drawing.Point(25, 255);
            this.BTTN_ChseGrade11.Name = "BTTN_ChseGrade11";
            this.BTTN_ChseGrade11.Size = new System.Drawing.Size(124, 47);
            this.BTTN_ChseGrade11.TabIndex = 5;
            this.BTTN_ChseGrade11.Text = "11. klase";
            this.BTTN_ChseGrade11.UseVisualStyleBackColor = true;
            // 
            // BTTN_ChseGrade12
            // 
            this.BTTN_ChseGrade12.Location = new System.Drawing.Point(25, 332);
            this.BTTN_ChseGrade12.Name = "BTTN_ChseGrade12";
            this.BTTN_ChseGrade12.Size = new System.Drawing.Size(124, 47);
            this.BTTN_ChseGrade12.TabIndex = 6;
            this.BTTN_ChseGrade12.Text = "12. klase";
            this.BTTN_ChseGrade12.UseVisualStyleBackColor = true;
            // 
            // BTTN_backToMain
            // 
            this.BTTN_backToMain.Location = new System.Drawing.Point(12, 413);
            this.BTTN_backToMain.Name = "BTTN_backToMain";
            this.BTTN_backToMain.Size = new System.Drawing.Size(154, 58);
            this.BTTN_backToMain.TabIndex = 7;
            this.BTTN_backToMain.Text = "Atpakaļ";
            this.BTTN_backToMain.UseVisualStyleBackColor = true;
            this.BTTN_backToMain.Click += new System.EventHandler(this.BTTN_backToMain_Click);
            // 
            // BTTN_viewDatabase
            // 
            this.BTTN_viewDatabase.Location = new System.Drawing.Point(554, 357);
            this.BTTN_viewDatabase.Name = "BTTN_viewDatabase";
            this.BTTN_viewDatabase.Size = new System.Drawing.Size(122, 50);
            this.BTTN_viewDatabase.TabIndex = 8;
            this.BTTN_viewDatabase.Text = "Meklēt uzdevumus datubāze";
            this.BTTN_viewDatabase.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(665, 76);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(58, 21);
            this.comboBox1.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(626, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Uzdevumu grūtība ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(638, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "No";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(757, 76);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(58, 21);
            this.comboBox2.TabIndex = 12;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(728, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "līdz";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(626, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Kontroldarba laiks";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(629, 136);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(94, 21);
            this.comboBox3.TabIndex = 15;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(731, 136);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(86, 20);
            this.textBox1.TabIndex = 16;
            // 
            // BTTN_GenSettings
            // 
            this.BTTN_GenSettings.Location = new System.Drawing.Point(682, 357);
            this.BTTN_GenSettings.Name = "BTTN_GenSettings";
            this.BTTN_GenSettings.Size = new System.Drawing.Size(122, 50);
            this.BTTN_GenSettings.TabIndex = 17;
            this.BTTN_GenSettings.Text = "Papiluds ģenerācijas iestatījumi";
            this.BTTN_GenSettings.UseVisualStyleBackColor = true;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(629, 26);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(186, 21);
            this.comboBox4.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(626, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Priekšmets";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(190, 39);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(96, 17);
            this.checkBox1.TabIndex = 20;
            this.checkBox1.Text = "8 klases temas";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(190, 169);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(102, 17);
            this.checkBox3.TabIndex = 22;
            this.checkBox3.Text = "10 klases temas";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.Visible = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(190, 101);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(96, 17);
            this.checkBox4.TabIndex = 23;
            this.checkBox4.Text = "9 klases temas";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.Visible = false;
            // 
            // TestGenMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 490);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.BTTN_GenSettings);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.BTTN_viewDatabase);
            this.Controls.Add(this.BTTN_backToMain);
            this.Controls.Add(this.BTTN_ChseGrade12);
            this.Controls.Add(this.BTTN_ChseGrade11);
            this.Controls.Add(this.BTTN_ChseGrade10);
            this.Controls.Add(this.BTTN_ChseGrade9);
            this.Controls.Add(this.BTTN_ChseGrade8);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TestGenMaster";
            this.Text = "TestGenMaster";
            this.Load += new System.EventHandler(this.TestGenMaster_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BTTN_ChseGrade8;
        private System.Windows.Forms.Button BTTN_ChseGrade9;
        private System.Windows.Forms.Button BTTN_ChseGrade10;
        private System.Windows.Forms.Button BTTN_ChseGrade11;
        private System.Windows.Forms.Button BTTN_ChseGrade12;
        private System.Windows.Forms.Button BTTN_backToMain;
        private System.Windows.Forms.Button BTTN_viewDatabase;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button BTTN_GenSettings;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
    }
}