﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace masterVersion
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private void BTTN_OpenTestGen_Click(object sender, EventArgs e)
        {
            TestGenMaster testGen = new TestGenMaster(this);
            testGen.Show();
            this.Hide();
        }

        private void BTTN_CloseMain_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BTTN_OpenTaskView_Click(object sender, EventArgs e)
        {
            PievienotUzdevumuMaster addTask = new PievienotUzdevumuMaster(this);
            addTask.Show();
            this.Hide();
        }
    }
}
